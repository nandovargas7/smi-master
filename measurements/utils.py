from django.utils.translation import gettext_lazy as _
from django.utils import timezone
from rest_framework.exceptions import APIException
from transductors.models import EnergyTransductor
from utils import ValidationException
from datetime import timedelta
from dateutil import relativedelta

class MeasurementParamsValidator():

    @staticmethod
    def get_fields():
        return [('id',
                 MeasurementParamsValidator.validate_id),
                ('start_date', MeasurementParamsValidator.validate_start_date),
                ('end_date', MeasurementParamsValidator.validate_end_date)]

    @staticmethod
    def validate_query_params(params, ignore=[]):
        fields = MeasurementParamsValidator.get_fields()
        errors = {}
        for field in fields:
            if field[0] not in ignore:
                try:
                    validation_function = field[1] 
                    param = params[field[0]]
                    validation_function(param)

                except KeyError:
                    errors[field[0]] = _(
                        'It must have an %s argument' % field[0]
                    )
                except ValidationException as e:
                    errors[field[0]] = e

        exception = APIException(
            errors,
            _('This id does not match with any Transductor'),
        )
        exception.status_code = 400
        if len(errors) != 0:
            raise exception

    @staticmethod
    def validate_id(transductor_id):
        try:        
            EnergyTransductor.objects.get(id=transductor_id)
        except EnergyTransductor.DoesNotExist:
            raise ValidationException(
                _('This id does not match with any Transductor'),
            )

    @staticmethod
    def validate_start_date(start_date):
        try:
            timezone.datetime.strptime(start_date, "%Y-%m-%d %H:%M:%S")
        except ValueError:
            message = 'The start_date param must be a valid date in '
            message += 'the format YYYY-MM-DD HH:MM:SS'
            raise ValidationException(
                _(message),
            )

    @staticmethod
    def validate_end_date(end_date):
        try:
            timezone.datetime.strptime(end_date, "%Y-%m-%d %H:%M:%S")
        except ValueError:
            message = 'The end_date param must be a valid date in '
            message += 'the format YYYY-MM-DD HH:MM:SS'
            raise ValidationException(
                _(message),
            )


class ConsumptionCalculator:
    def calculate_total_consumption_per_hour(self, measurements, field):
        total_consumption_per_hour = []
        return total_consumption_per_hour


class ConsumptionCurveDataService:
    def __init__(self, queryset, fields, start_date, end_date, campus, group, period):
        self.queryset = queryset
        self.fields = fields
        self.start_date = start_date
        self.end_date = end_date
        self.campus = campus
        self.group = group
        self.period = period

    def mount_data_list(self):
        if self.period == 'hourly':
            return self.hourly_consumption_format()
        elif self.period == 'daily':
            return self.daily_consumption_format()
        elif self.period == 'monthly':
            return self.monthly_consumption_format()

    def hourly_consumption_format(self):
        hourly_consumption = {}

        for i in range(24):
            hourly_consumption[str(i) + 'h'] = 0

        for field in self.fields:
            measurements = self.queryset.values(field, 'collection_date')
            for measurement in measurements:
                hour = measurement['collection_date'].hour
                hourly_consumption[str(hour) + 'h'] += measurement[field]

        return hourly_consumption

    def daily_consumption_format(self):
        daily_consumption = {}
        delta = self._get_delta()
        current_date = self._get_start_date()

        for i in range(abs(int(delta.days)) + 1):
            daily_consumption[str(current_date.strftime('%d/%m/%y'))] = 0
            current_date = current_date + timedelta(days=1)

        for field in self.fields:
            measurement = measurements = self.queryset.values(field, 'collection_date')
            for measurement in measurements:
                day = str(measurement['collection_date'].strftime('%d/%m/%y'))
                daily_consumption[day] += measurement[field]

        return daily_consumption

    def monthly_consumption_format(self):
        monthly_consumption = {}
        delta = self._get_delta()
        current_date = self._get_start_date()

        for i in range(abs(int(delta.months)) + 1):
            monthly_consumption[str(current_date.strftime('%m/%y'))] = 0
            current_date = current_date + relativedelta.relativedelta(months=1)

        for field in self.fields:
            measurement = measurements = self.queryset.values(field, 'collection_date')
            for measurement in measurements:
                month = str(measurement['collection_date'].strftime('%m/%y'))
                monthly_consumption[month] += measurement[field]

        return monthly_consumption

    def _get_delta(self):
        return self.end_date - self.start_date

    def _get_start_date(self):
        return self.start_date